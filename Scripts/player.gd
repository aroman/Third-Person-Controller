extends CharacterBody3D

@export_category("Character Variables")
@export var speed: float = 5.0
@export var jump_height: float = 5.0
@export var gravity: float = 9.8

@export_category("Camera Variables")
@export var mouse_sense: float = 0.005
@export var cam_up_max: float = 20.0
@export var cam_down_max: float = -80.0

@onready var head: Node3D = $Head

func _ready() -> void:
	# Capture the mouse
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func _input(event: InputEvent) -> void:
	# Handle mouse movement
	if event is InputEventMouseMotion:
		rotate_y(-event.relative.x * mouse_sense)
		head.rotate_x(-event.relative.y * mouse_sense)
		head.rotation.x = clamp(head.rotation.x, deg_to_rad(cam_down_max), deg_to_rad(cam_up_max))

func _physics_process(delta: float) -> void:
	# Gravity
	if not is_on_floor():
		velocity.y -= gravity * delta

	# Jump
	if Input.is_action_just_pressed("jump") and is_on_floor():
		velocity.y = jump_height

	# Handle movement
	var input_dir := Input.get_vector("left", "right", "forward", "backward")
	var direction := (transform.basis * Vector3(input_dir.x, 0, input_dir.y)).normalized()
	if direction:
		velocity.x = direction.x * speed
		velocity.z = direction.z * speed
	else:
		velocity.x = move_toward(velocity.x, 0, speed)
		velocity.z = move_toward(velocity.z, 0, speed)

	move_and_slide()
